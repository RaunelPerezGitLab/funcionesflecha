const nombres = ['Stephanie','Obed','Ulises','Raunel'];

//se va a crear una función que cuente la cantidad de caracteres que tiene cada nombre
//Esta forma que se muestra aquí es la vieja foema de usarla funciones donde se usa la forma function(parametro){cuerpo}
const cant_caracteres = nombres.map(function(nombre){
    console.log(nombre,' tiene', nombre.length, 'caracteres');
});


//Ahora hay que mencionar la estructura de la funciones tipo flecha: (parameters) =>{code & return}

//Creando una funcion de tipo flecha
const print_nombres = nombres.map((nombre)=>{
    console.log(nombre);
}); 

//Creando la funcion tipo flecha optimizada, estructura parameter => code
const print_nombres_opt =nombres.map(nombre => console.log('nombre: ',nombre));